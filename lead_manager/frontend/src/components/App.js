import React, { Component, Fragment } from "react";
import ReactDOM from "react-dom";
import { HashRouter as Router, Route, Routes, Redirect } from "react-router-dom";
import { Provider as AlertProvider } from "react-alert";
import AlertTemplate from "react-alert-template-basic";
import { Provider } from "react-redux";
import store from "../store";

import PrivateRoute from "./common/PrivateRoute";
import Header from "./layout/Header";
import Alerts from "./layout/Alerts";
import Dashboard from "./leads/Dashboard";
import Register from "./accounts/Register"
import Login from "./accounts/Login";

import { loadUser } from "../actions/auth";

const alertOptions = {
	timeout: 3000,
	position: "top center",
};

class App extends Component {

	componentDidMount() {
		store.dispatch(loadUser());
	};

	render() {
		return (
			<Provider store={store}>
				<AlertProvider template={AlertTemplate} {...alertOptions}>
					<Router>
						<Fragment>
							<Header />
							<Alerts />
							<div className="container">
								<Routes>
									<Route element={<PrivateRoute />} >
										<Route path="/" element={<Dashboard />} />
									</Route>
									<Route exact path="/register" element={<Register />} />
									<Route exact path="/login" element={<Login />} />
								</Routes>
							</div>
						</Fragment>
					</Router>
				</AlertProvider>
			</Provider>
		);
	};
};

ReactDOM.render(<App />, document.getElementById('app'));

