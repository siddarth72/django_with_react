import React from 'react';
import { Navigate, Outlet } from 'react-router-dom';
import { useSelector } from 'react-redux';

const PrivateRoute = ({ element: Component, ...rest }) => {

	const auth = useSelector((state) => state.auth);

	return auth.isLoading ? (
		<div>Loading...</div>
	) : !auth.isAuthenticated ? (
		<Navigate to="/login" />
	) : (
		<Outlet />
	);
};

export default PrivateRoute;

